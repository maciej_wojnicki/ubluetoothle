Blue-Rx is a Bluetooth Low Energy 16 channel servo controller by Star Technologies.  Use it to control RC cars, planes, robots, or any servo from your iPhone or Android device.

It has the ability to control 16 standard servos or generic PWM devices, read 3 ADC values, and switch 2 digital outputs.

For more information see:
http://repo.startechplus.com/bluerx

To purchase a Blue-Rx goto:
http://www.gigamint.com

Power up the Blue-Rx, press Scan, then press Connect... use the sliders or the accelerometer to conntrol the servos on channel 1 or 2.